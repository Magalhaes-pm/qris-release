import time
import serial
import RPi.GPIO as GPIO
from neopixel import *
import argparse

print "Make-IT_prototype test"

#pinout
buzzer = 29				
fan = 7
GreenLED = 33
YellowLED = 37

GPIO.setwarnings(False)			#disable warnings
GPIO.setmode(GPIO.BOARD)		#set pin numbering system
GPIO.setup(buzzer,GPIO.OUT)
GPIO.setup(fan,GPIO.OUT)
GPIO.setup(GreenLED,GPIO.OUT)
GPIO.setup(YellowLED,GPIO.OUT)

nOfBips_acepted = 2
nOfBips_denied = 2
accepted_biptime = 0.3          #bip time in seconds
denied_biptime = 0.1            #bip time in seconds

#gpio init
GPIO.output(GreenLED, GPIO.LOW)
GPIO.output(YellowLED, GPIO.LOW)
GPIO.output(fan,GPIO.HIGH)
time.sleep(1)
pi_pwm = GPIO.PWM(buzzer,50)		#create PWM instance with frequency
pi_pwm.stop()				        #start PWM of required Duty Cycle 

# LED strip configuration:
LED_COUNT      = 24      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()

#Serial
data_accepted = "0bd45701344a7fa0d64443edc8d93d00e0d55c0ff322a715415ebc857841ec4e"  #QRcode name: frame1 

ser = serial.Serial('/dev/ttyS0', baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=0.01
                    )
time.sleep(1)

#buzzer bip
GPIO.output(buzzer, GPIO.HIGH)
time.sleep(0.5)
GPIO.output(buzzer, GPIO.LOW)

#loop

start_time = time.time()
s = 0
try:
    while True:
        #print "Apresente o smartphone"

        #loop animation
        if time.time() - start_time > 0.05:
            #ligar proximo led
            
            for i in range(strip.numPixels()):
                strip.setPixelColor(i,Color(0,0,0))
            strip.show()
            strip.setPixelColor(s, Color(127,127,127))
            strip.show()
            time.sleep(0.001)
            start_time = time.time()
            if s<24:
                s = s+1
            else:
                s = 0
                strip.setPixelColor(s, Color(127,127,127))
                strip.show()
            
        
        data = ser.readline(64)
        if len(data) == 64:
            #print data
            if data == data_accepted:
                #QRcode aceite
                #Accepted animation
                GPIO.output(buzzer, GPIO.HIGH)

                for i in range(strip.numPixels()):
                    strip.setPixelColor(i,Color(255,0,0))
                    strip.show()
                    time.sleep(0.013)
                
                GPIO.output(buzzer, GPIO.LOW)
                                    
                print "Bilhete valido"
                print "Proximo Passageiro"
            
            else:
                #QRcode rejeitado
                for x in range (0,nOfBips_denied):        
                    pi_pwm.start(50)				        #start PWM of required Duty Cycle

                    for i in range(strip.numPixels()):
                        strip.setPixelColor(i,Color(0,255,0))
                        strip.show()
                        time.sleep(2/1000.0)
                    time.sleep(0.05)     
                    
                    pi_pwm.stop()
                    for i in range(strip.numPixels()):
                        strip.setPixelColor(i,Color(0,0,0))
                        strip.show()
                        time.sleep(2/1000.0)
                    time.sleep(0.05)
                
                print "Bilhete invalido!"
                time.sleep(0.2)
        
except KeyboardInterrupt:
    print "Exiting Program"
    #pi_pwm_fan.stop()
    GPIO.output(GreenLED,GPIO.LOW)
    GPIO.output(YellowLED,GPIO.LOW)
    GPIO.output(fan,GPIO.LOW)
    for i in range(strip.numPixels()):
        strip.setPixelColor(i,Color(0,0,0))
        strip.show()

except:
    print "Error Occurs, Exiting Program"

finally:
    ser.close()
    pass